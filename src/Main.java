/**
 * this is runner for this project
 * @author Elham Bastami
 * @version 1.0
 * @since 2020-08-28
 */

import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;

public class Main {
        public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException {
            NodeList nList = ManageXMLFile.getDocument("C:\\Users\\m-pc\\Desktop\\123.xml");
            ArrayList<Account> accounts=ManageXMLFile.setDocument(nList);
            ManageXMLFile.showResult(accounts);


        }
    }


