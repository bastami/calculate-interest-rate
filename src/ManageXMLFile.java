/**
 * this class manage XML File in three section
 * 1. read File
 * 2. set file and calculate payed interest
 * 3.show data
 * @author Elham Bastami
 * @version 1.0
 * @since 2020-08-28
 */
import javax.xml.parsers.DocumentBuilder;
import com.sun.deploy.security.SelectableSecurityManager;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

class ManageXMLFile {
    /**
     * ،this method get a list of nodes and build class by reflection
     * @param nList
     * @return a list of object
     */
    public static ArrayList setDocument(NodeList nList) {
        ArrayList<Account> finalListOfAccount=new ArrayList<>();
        BigDecimal payedInterest;
        try {
            System.out.println("----------------------------");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
               // System.out.println("\nCurrent Element :" + nNode.getNodeName());
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    BigDecimal depBalance = new BigDecimal(eElement.getElementsByTagName("depositBalance").item(0).getTextContent());
                    int drnInDay = Integer.parseInt(eElement.getElementsByTagName("durationInDays").item(0).getTextContent());
                    int cstNumber = Integer.parseInt(eElement.getElementsByTagName("customerNumber").item(0).getTextContent());
                    String depType = eElement.getElementsByTagName("depositType").item(0).getTextContent();
                    if (checkDepositBalance(depBalance) == true && checkDurationInDay(drnInDay) == true) {
                        if (Qarz.class.getSimpleName().equals(depType)) {
                            Account qarz = new Qarz();
                            payedInterest=qarz.calculateInterestRate(depBalance, drnInDay);
                            finalListOfAccount.add(setterMethod(depBalance, drnInDay, cstNumber, depType, payedInterest, qarz));
                        } else if (LongTerm.class.getSimpleName().equals(depType)) {
                            Account longTerm = new LongTerm();
                            payedInterest=longTerm.calculateInterestRate(depBalance, drnInDay);
                            finalListOfAccount.add(setterMethod(depBalance, drnInDay, cstNumber, depType, payedInterest, longTerm));
                        } else if (ShortTerm.class.getSimpleName().equals(depType)) {
                            Account shortTerm = new ShortTerm();
                            payedInterest=shortTerm.calculateInterestRate(depBalance, drnInDay);
                            finalListOfAccount.add(setterMethod(depBalance, drnInDay, cstNumber, depType, payedInterest,shortTerm ));
                        } else {
                            System.out.println("In one row,One  of *Deposit type* is invalid");

                        }

                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return finalListOfAccount;
    }

    /**
     * This method values ​​the properties of the account class through reflection
     * @param depBalance
     * @param drnInDay
     * @param cstNumber
     * @param depType
     * @param payedInterest
     * @param account
     * @return
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    private static Account setterMethod(BigDecimal depBalance, int drnInDay, int cstNumber, String depType,BigDecimal payedInterest, Account account) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Method setterDurationInDay = Account.class.getMethod("setDurationInDays", int.class);
        setterDurationInDay.invoke(account, drnInDay);

        Method setterCustomerNumber = Account.class.getMethod("setCustomerNumber", int.class);
        setterCustomerNumber.invoke(account, cstNumber);

        Method setterDepositType = Account.class.getMethod("setDepositType", String.class);
        setterDepositType.invoke(account, depType);

        Method setterDepositBalance = Account.class.getMethod("setDepositBalance", BigDecimal.class);
        setterDepositBalance.invoke(account, depBalance);

        Method setterPayedInterest = Account.class.getMethod("setPayedInterest", BigDecimal.class);
        setterPayedInterest.invoke(account, payedInterest);
       return account;

    }

    /**
     * This method receives the address of an XML file and returns all its information as a list of node
     * @param adress
     * @return a node list
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */

    static NodeList getDocument(String adress) throws ParserConfigurationException, SAXException, IOException {
        File inputFile = new File(adress);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(inputFile);
        doc.getDocumentElement().normalize();
      //  System.out.println("Root element :"+ doc.getDocumentElement().getNodeName());
        NodeList nodeList = doc.getElementsByTagName("deposit");
        return nodeList;
    }

    /**
     *
     * @param bigDecimal
     * @return true if deposit Balance greater than zero
     */
    private static boolean checkDepositBalance(BigDecimal bigDecimal) {
        if (bigDecimal.compareTo(new BigDecimal(0)) == 1)
            return true;
        else {
            System.out.println("In one row, the *deposit balance* is less than zero");
            return false;
        }

    }

    /**
     * this method get a List than contain account information and sort this information by payed interest and show it
     * @param accounts is contain account information
     */
    public static void showResult(ArrayList<Account> accounts){
        List<Account> sortedList=accounts.stream().sorted(Comparator.comparing(Account::getPayedInterest).reversed())
                .collect(Collectors.toList());
        sortedList.forEach(System.out::println);
    }

    /**
     * This method checks the validity of the input value
     * @param durationInDays
     * @return  true if the input value is greater than zero
     */
    private static boolean checkDurationInDay(int durationInDays) {
        if (durationInDays >= 0)
            return true;
        else {
            System.out.println("In one row, *the duration in day*  is less than zero");
            return false;
        }

    }


}
