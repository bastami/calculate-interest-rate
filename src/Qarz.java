/**
 * This is Qarz class
 * @author Elham Bastami
 * @version 1.0
 * @since 2020-08-28
 */
import java.math.BigDecimal;
import java.math.RoundingMode;

public class Qarz extends Account{
    private static int interestRate=0;
    /**
     *this method calculate payed interest for longterm account
     * @param depositBalance This value represents deposit Balance
     * @param durationInDays This value represents duration in day
     * @return a BigDecimal number that return payed Interest
     */
    @Override
    public BigDecimal calculateInterestRate(BigDecimal depositBalance, int durationInDays) {
        return (((depositBalance.multiply(new BigDecimal( durationInDays)))
                .multiply(new BigDecimal(interestRate))).divide(BigDecimal.valueOf(36500),2, RoundingMode.HALF_UP));

    }
}
