import java.math.BigDecimal;
/**
 * This class contains account information
 * @author Elham Bastami
 * @version 1.0
 * @since 2020-08-28
 */

public abstract class Account {

    private int customerNumber;
    private String depositType;
    private BigDecimal depositBalance;
    private int durationInDays;
    private  int interestRate;
    private BigDecimal payedInterest;

    public int getCustomerNumber() {
        return customerNumber;
    }

    public BigDecimal getPayedInterest() {
        return payedInterest;
    }

    public void setPayedInterest(BigDecimal payedInterest) {
        this.payedInterest = payedInterest;
    }

    public void setCustomerNumber(int customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getDepositType() {
        return depositType;
    }

    public void setDepositType(String depositType) {
        this.depositType = depositType;
    }

    public BigDecimal getDepositBalance() {
        return depositBalance;
    }

    public void setDepositBalance(BigDecimal depositBalance) {
        this.depositBalance = depositBalance;
    }

    public int getDurationInDays() {
        return durationInDays;
    }

    public void setDurationInDays(int durationInDays) {
        this.durationInDays = durationInDays;
    }
    public abstract BigDecimal calculateInterestRate(BigDecimal depositBalance,int durationInDays);

    @Override
    public String toString() {
        return customerNumber+"#"+payedInterest;
    }
}
