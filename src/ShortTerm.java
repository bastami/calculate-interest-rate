/**
 * this is short term account class
 * @author Elham Bastami
 * @version 1.0
 * @since 2020-08-28
 */
import java.math.BigDecimal;
import java.math.RoundingMode;

public class ShortTerm extends Account {
    private  int interestRate=10;
    /**
     *this method calculate payed interest for longterm account
     * @param depositBalance This value represents deposit Balance
     * @param durationInDays This value represents duration in day
     * @return a BigDecimal number that return payed Interest
     */
    @Override
    public BigDecimal calculateInterestRate(BigDecimal depositBalance, int durationInDays) {
        //System.out.println(depositBalance.divide(BigDecimal.valueOf(365000),2, RoundingMode.HALF_UP));

        return (((depositBalance.multiply(new BigDecimal( durationInDays)))
                             .multiply(new BigDecimal(interestRate))).divide(BigDecimal.valueOf(36500),4, RoundingMode.HALF_UP));


    }
}
